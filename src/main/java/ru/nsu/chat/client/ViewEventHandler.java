package ru.nsu.chat.client;

import ru.nsu.chat.common.message.server.ServerMessage;

public interface ViewEventHandler {
    void handle(ServerMessage message);
}
