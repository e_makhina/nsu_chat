package ru.nsu.chat.client.frame;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.nsu.chat.client.*;
import ru.nsu.chat.common.ListedUser;
import ru.nsu.chat.common.message.client.ClientMessageRequest;
import ru.nsu.chat.common.message.server.*;

import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.DefaultCaret;
import java.awt.*;
import java.awt.event.*;
import java.util.Collections;
import java.util.HashMap;
import java.util.Observable;
import java.util.concurrent.CopyOnWriteArrayList;

public class MainFrame implements ClientFrame {
    private static final Logger logger = LoggerFactory.getLogger(MainFrame.class);

    private static Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
    private static final int FRAME_WIDTH = 680;
    private static final int FRAME_HEIGHT = 420;
    private Client client;
    private JFrame frame;
    private MainPanel contentPane;

    public MainFrame(final Client client) {
        this.client = client;
        frame = new JFrame("Chat client");
        frame.setBackground(Color.LIGHT_GRAY);
        int dimX = ((int) screenSize.getWidth() - FRAME_WIDTH) / 2;
        int dimY = ((int) screenSize.getHeight() - FRAME_HEIGHT) / 2;
        frame.setBounds(dimX, dimY, FRAME_WIDTH, FRAME_HEIGHT);
        frame.setResizable(false);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                logger.info("closing chat window");
                client.disconnect();
                System.exit(0);
            }
        });

        contentPane = new MainPanel();
        contentPane.setBackground(Color.LIGHT_GRAY);
        contentPane.setOpaque(true);
        frame.setContentPane(contentPane);

        frame.setVisible(true);
    }

    public MainPanel getContentPane() {
        return contentPane;
    }

    public class MainPanel extends JPanel implements ClientContentPane {
        private JTextArea userList;
        private JTextArea chatWindow;
        private JTextArea messageArea;
        private JButton sendButton;
        private CopyOnWriteArrayList<String> usernames;
        private HashMap<ServerEvent, ViewEventHandler> eventHandlers;

        public MainPanel() {
            logger.info("Creating main frame");
            usernames = new CopyOnWriteArrayList<>();
            eventHandlers = new HashMap<>();
            eventHandlers.put(ServerEvent.SUCCESS_USERLIST_RESPONSE, new SuccessUserListHandler());
            eventHandlers.put(ServerEvent.SUCCESS_LOGOUT_RESPONSE, new SuccessLogoutHandler());
            eventHandlers.put(ServerEvent.NEW_TEXT_MESSAGE, new EventUserMessageHandler());
            eventHandlers.put(ServerEvent.NEW_USER_LOGIN, new EventUserLoginHandler());
            eventHandlers.put(ServerEvent.NEW_USER_LOGOUT, new EventUserLogoutHandler());
            eventHandlers.put(ServerEvent.CONNECTION_ERROR, new ErrorConnectionHandler());
            eventHandlers.put(ServerEvent.ERROR_MESSAGE_RESPONSE, new ErrorMessageHandler());
            eventHandlers.put(ServerEvent.ERROR_USERLIST_RESPONSE, new ErrorUserListHandler());
            eventHandlers.put(ServerEvent.ERROR_LOGOUT_RESPONSE, new ErrorLogoutHandler());
            logger.info("initialized event eventHandlers");


            GridBagLayout gbl = new GridBagLayout();
            GridBagConstraints constraints = new GridBagConstraints();

            constraints.gridx = 0;
            constraints.gridy = 0;
            constraints.gridwidth = 2;
            constraints.gridheight = 5;
            constraints.anchor = GridBagConstraints.NORTHEAST;
            constraints.insets = new Insets(20, 20, 10, 2);
            userList = new JTextArea(20, 15);
            userList.setEditable(false);
            userList.setFont(new Font(Font.DIALOG, Font.BOLD, 12));
            JScrollPane userPanel = new JScrollPane(userList);
            userPanel.setBorder(BorderFactory.createMatteBorder(5, 5, 5, 5, Color.WHITE));
            gbl.setConstraints(userPanel, constraints);
            add(userPanel);

            constraints.gridx = 2;
            constraints.gridy = 0;
            constraints.gridwidth = 6;
            constraints.gridheight = 5;
            constraints.anchor = GridBagConstraints.NORTHWEST;
            constraints.insets = new Insets(20, 2, 10, 20);
            chatWindow = new JTextArea(20, 40);
            chatWindow.setEditable(false);
            chatWindow.setLineWrap(true);
            chatWindow.setWrapStyleWord(true);
            JScrollPane chatPanel = new JScrollPane(chatWindow);
            chatPanel.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
            chatPanel.setBorder(BorderFactory.createMatteBorder(5, 5, 5, 5, Color.WHITE));
            gbl.setConstraints(chatPanel, constraints);
            add(chatPanel);

            constraints.gridx = 0;
            constraints.gridy = 5;
            constraints.gridwidth = 6;
            constraints.gridheight = 1;
            constraints.anchor = GridBagConstraints.NORTHEAST;
            constraints.insets = new Insets(10, 20, 20, 10);
            messageArea = new JTextArea(4, 51);
            JScrollPane messagePanel = new JScrollPane(messageArea);
            messagePanel.setBorder(BorderFactory.createMatteBorder(5, 5, 5, 5, Color.WHITE));
            messageArea.setText("Write a message...");
            messageArea.setLineWrap(true);
            messageArea.setWrapStyleWord(true);
            DefaultCaret caret = (DefaultCaret) messageArea.getCaret();
            caret.setUpdatePolicy(DefaultCaret.ALWAYS_UPDATE);
            messageArea.addKeyListener(new KeyAdapter() {

                @Override
                public void keyPressed(KeyEvent e) {
                    if (e.getKeyCode() == KeyEvent.VK_ENTER) {
                        e.consume();
                        sendButton.doClick();
                    }
                }
            });
            gbl.setConstraints(messagePanel, constraints);
            add(messagePanel);


            constraints.gridx = 6;
            constraints.gridy = 5;
            constraints.gridwidth = 2;
            constraints.gridheight = 1;
            constraints.anchor = GridBagConstraints.NORTHWEST;
            constraints.insets = new Insets(10, 10, 20, 20);
            sendButton = new JButton("Send");
            sendButton.setEnabled(false);
            gbl.setConstraints(sendButton, constraints);
            add(sendButton);

            sendButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent actionEvent) {
                    EventQueue.invokeLater(new Runnable() {
                        @Override
                        public void run() {
                            client.sendMessage(new ClientMessageRequest(messageArea.getText(), client.getID()));
                            messageArea.setText("");
                        }
                    });
                }
            });


            messageArea.getDocument().addDocumentListener(new DocumentListener() {
                @Override
                public void insertUpdate(DocumentEvent documentEvent) {
                    if (documentEvent.getDocument().getLength() > 0) {
                        EventQueue.invokeLater(new Runnable() {
                            @Override
                            public void run() {
                                sendButton.setEnabled(true);
                            }
                        });
                    }
                }

                @Override
                public void removeUpdate(DocumentEvent documentEvent) {
                    if (documentEvent.getDocument().getLength() == 0) {
                        EventQueue.invokeLater(new Runnable() {
                            @Override
                            public void run() {
                                sendButton.setEnabled(false);
                            }
                        });
                    }
                }

                @Override
                public void changedUpdate(DocumentEvent documentEvent) {
                }
            });

            messageArea.addFocusListener(new FocusAdapter() {
                @Override
                public void focusGained(FocusEvent e) {
                    EventQueue.invokeLater(new Runnable() {
                        @Override
                        public void run() {
                            messageArea.setText("");
                        }
                    });
                }

                @Override
                public void focusLost(FocusEvent e) {
                    if (messageArea.getDocument().getLength() == 0) {
                        EventQueue.invokeLater(new Runnable() {
                            @Override
                            public void run() {
                                messageArea.setText("Write a message...");
                                sendButton.setEnabled(false);
                            }
                        });
                    }

                }
            });

            logger.info("Set up all GUI");
        }

        private synchronized void rewriteUserList() {
            EventQueue.invokeLater(new Runnable() {
                @Override
                public void run() {
                    if (!usernames.isEmpty()) {
                        Collections.sort(usernames);
                    }
                    userList.setText("");

                    for (String name : usernames) {
                        userList.append(name + "\n\n");
                    }

                    logger.info("Rewriting user list area:");
                }
            });
        }


        private void writeToChatArea(final String message) {
            EventQueue.invokeLater(new Runnable() {
                @Override
                public void run() {
                    chatWindow.append(message + "\n");
                    chatWindow.setCaretPosition(chatWindow.getDocument().getLength());
                }
            });
        }

        @Override
        public void update(Observable obs, Object event) {
            if (event instanceof ObservedEvent) {
                ObservedEvent newEvent = (ObservedEvent) event;
                ViewEventHandler handler = eventHandlers.get(newEvent.getEventType());
                if (handler != null) {
                    handler.handle(newEvent.getMessage());
                }
            }
            logger.info("Got new " + event.getClass().getName());
        }

        class EventUserLoginHandler implements ViewEventHandler {
            @Override
            public void handle(ServerMessage message) {
                ServerMessageUserLogin event = (ServerMessageUserLogin) message;
                writeToChatArea("\nNew user " + event.getUsername() + " has connected.\n");

                if (!usernames.contains(event.getUsername())) {
                    usernames.add(event.getUsername());
                }
                rewriteUserList();
            }
        }

        class EventUserLogoutHandler implements ViewEventHandler {
            @Override
            public void handle(ServerMessage message) {
                ServerMessageUserLogout event = (ServerMessageUserLogout) message;
                writeToChatArea("\nUser " + event.getUsername() + " has left.\n");
                usernames.remove(event.getUsername());
                rewriteUserList();
            }
        }

        class EventUserMessageHandler implements ViewEventHandler {
            @Override
            public void handle(ServerMessage message) {
                ServerMessageText event = (ServerMessageText) message;
                writeToChatArea(event.getUsername() + ": " + event.getMessage());
            }
        }

        class SuccessLogoutHandler implements ViewEventHandler {
            @Override
            public void handle(ServerMessage message) {
                client.disconnect();
                System.exit(0);
            }
        }

        class SuccessUserListHandler implements ViewEventHandler {
            @Override
            public void handle(ServerMessage message) {
                final ServerSuccessUserListResponse event = (ServerSuccessUserListResponse) message;
                EventQueue.invokeLater(new Runnable() {
                    @Override
                    public void run() {
                        usernames.clear();
                        for (ListedUser user : event.getUsers()) {
                            if (!user.getUsername().equals(client.getUsername())) {
                                usernames.add(user.getUsername());
                            }
                        }
                        rewriteUserList();
                    }
                });
            }
        }

        class ErrorLogoutHandler implements ViewEventHandler {
            @Override
            public void handle(ServerMessage message) {
                final ServerErrorResponse event = (ServerErrorResponse) message;
                EventQueue.invokeLater(new Runnable() {
                    @Override
                    public void run() {
                        JOptionPane.showMessageDialog(frame, event.getReason(), "Error", JOptionPane.ERROR_MESSAGE);
                        client.disconnect();
                        System.exit(0);
                    }
                });
            }

        }

        class ErrorMessageHandler implements ViewEventHandler {
            @Override
            public void handle(ServerMessage message) {
                writeToChatArea("< Some message has not been sent >");
            }
        }

        class ErrorUserListHandler implements ViewEventHandler {
            @Override
            public void handle(ServerMessage message) {
                writeToChatArea("< Could not get actual list of users >");
                userList.setText("");
            }

        }

        class ErrorConnectionHandler implements ViewEventHandler {
            @Override
            public void handle(ServerMessage message) {
                EventQueue.invokeLater(new Runnable() {
                    @Override
                    public void run() {
                        JOptionPane.showMessageDialog(frame,
                                "Server is down. Please try again.", "Error", JOptionPane.ERROR_MESSAGE);
                        client.disconnect();
                        frame.dispose();
                        client.proceedLoginFrame();
                    }
                });
            }
        }
    }
}
