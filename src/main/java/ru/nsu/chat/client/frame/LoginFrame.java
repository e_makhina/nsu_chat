package ru.nsu.chat.client.frame;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.nsu.chat.client.*;
import ru.nsu.chat.common.Configuration;
import ru.nsu.chat.common.message.client.ClientLoginRequest;
import ru.nsu.chat.common.message.server.ServerErrorResponse;
import ru.nsu.chat.common.message.server.ServerMessage;
import ru.nsu.chat.common.message.server.ServerSuccessLoginResponse;

import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.util.HashMap;
import java.util.Observable;

public class LoginFrame implements ClientFrame {
    private static final Logger logger = LoggerFactory.getLogger(LoginFrame.class);

    private static final int FRAME_WIDTH = 350;
    private static final int FRAME_HEIGHT = 330;
    private static Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();

    private JFrame frame;
    private LoginPanel contentPane;
    private Client client;

    public LoginFrame(Client client) {
        this.client = client;
        createAndShowGUI();
    }

    public LoginPanel getContentPane() {
        return contentPane;
    }

    private void createAndShowGUI() {
        frame = new JFrame("Chat client");
        int dimX = ((int) screenSize.getWidth() - FRAME_WIDTH) / 2;
        int dimY = ((int) screenSize.getHeight() - FRAME_HEIGHT) / 2;
        frame.setBounds(dimX, dimY, FRAME_WIDTH, FRAME_HEIGHT);
        frame.setResizable(false);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        contentPane = new LoginPanel();
        contentPane.setOpaque(true);
        frame.setContentPane(contentPane);

        frame.pack();
        frame.setVisible(true);
    }

    public class LoginPanel extends JPanel implements ClientContentPane {
        Configuration configuration = Configuration.parse();
        private final JTextField ip;
        private final JTextField port;
        private final JTextField username;
        private final JButton button;
        private FieldsListener listener = new FieldsListener();
        private HashMap<ServerEvent, ViewEventHandler> eventHandlers;

        public LoginPanel() {
            eventHandlers = new HashMap<>();
            eventHandlers.put(ServerEvent.CONNECTION_ERROR, new ConnectionErrorHandler());
            eventHandlers.put(ServerEvent.SUCCESS_LOGIN_RESPONSE, new SuccessLoginHandler());
            eventHandlers.put(ServerEvent.ERROR_LOGIN_RESPONSE, new ErrorLoginHandler());
            setLayout(new GridLayout(4, 1));
            setBackground(Color.LIGHT_GRAY);

            JPanel usernamePanel = new JPanel();
            username = new JTextField(15);
            username.setFont(new Font(Font.SANS_SERIF, Font.BOLD, 18));
            usernamePanel.setBackground(Color.LIGHT_GRAY);
            username.setBorder(BorderFactory.createEmptyBorder(10, 5, 10, 5));
            username.setToolTipText("Username may contain 'a'-'z', 'A'-'Z', '0'-'9' and " +
                    "'_' letters only (4-9 letters) ");
            username.setHorizontalAlignment(SwingConstants.CENTER);
            username.getDocument().addDocumentListener(listener);
            usernamePanel.add(username);
            add(usernamePanel);

            JPanel addressPanel = new JPanel();
            addressPanel.setBackground(Color.LIGHT_GRAY);
            ip = new JTextField("ip address", 10);
            ip.getDocument().addDocumentListener(listener);
            ip.addFocusListener(new FocusListener() {
                @Override
                public void focusGained(FocusEvent focusEvent) {
                    ip.setText("");
                }

                @Override
                public void focusLost(FocusEvent focusEvent) {
                    if (ip.getText().equals("")) {
                        ip.setText("ip address");
                    }
                }
            });
            ip.setHorizontalAlignment(SwingConstants.CENTER);
            ip.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
            addressPanel.add(ip);
            port = new JTextField("port", 4);
            port.setEditable(false);
            port.setHorizontalAlignment(SwingConstants.CENTER);
            port.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
            System.out.println(String.valueOf(configuration.getXMLPort()));
            port.setText(String.valueOf(configuration.getXMLPort()));
            addressPanel.add(port);
            add(addressPanel);

            JPanel buttonPanel = new JPanel();
            buttonPanel.setBackground(Color.LIGHT_GRAY);
            button = new JButton("Login");
            button.setPreferredSize(new Dimension(90, 40));
            button.setEnabled(false);
            button.setBorder(BorderFactory.createMatteBorder(2, 2, 2, 2, Color.BLACK));
            buttonPanel.add(button);
            add(buttonPanel);

            button.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent actionEvent) {
                    button.setEnabled(false);
                    client.connect(ip.getText(), Integer.parseInt(port.getText()), username.getText());
                    client.sendMessage(new ClientLoginRequest(username.getText()));
                }
            });
        }

        @Override
        public void update(Observable observable, Object event) {
            logger.info("Got new event");
            if (event instanceof ObservedEvent) {
                ObservedEvent newEvent = (ObservedEvent) event;
                ViewEventHandler handler = eventHandlers.get(newEvent.getEventType());

                if (handler != null) {
                    handler.handle(newEvent.getMessage());
                }
            }
        }

        class SuccessLoginHandler implements ViewEventHandler {
            @Override
            public void handle(ServerMessage message) {
                if (message instanceof ServerSuccessLoginResponse) {
                    ServerSuccessLoginResponse event = (ServerSuccessLoginResponse) message;
                    client.setID(event.getID());
                    frame.dispose();
                    client.proceedMainFrame();
                }
            }
        }

        class ErrorLoginHandler implements ViewEventHandler {
            @Override
            public void handle(ServerMessage message) {
                if (message instanceof ServerErrorResponse) {
                    final ru.nsu.chat.common.message.server.ServerErrorResponse errorResponse = (ServerErrorResponse) message;
                    username.setText("");

                    EventQueue.invokeLater(new Runnable() {
                        @Override
                        public void run() {
                            JOptionPane.showMessageDialog(frame, errorResponse.getReason(),
                                    "Error", JOptionPane.ERROR_MESSAGE);
                        }
                    });
                    client.disconnect();
                }
            }
        }

        class ConnectionErrorHandler implements ViewEventHandler {
            @Override
            public void handle(ServerMessage message) {
                EventQueue.invokeLater(new Runnable() {
                    @Override
                    public void run() {
                        JOptionPane.showMessageDialog(frame, "Disconnected.",
                                "Error", JOptionPane.ERROR_MESSAGE);
                    }
                });
            }
        }

        class FieldsListener implements DocumentListener {
            @Override
            public void insertUpdate(DocumentEvent documentEvent) {
                checkFields();
            }

            @Override
            public void removeUpdate(DocumentEvent documentEvent) {
                checkFields();
            }

            @Override
            public void changedUpdate(DocumentEvent documentEvent) {
                checkFields();
            }

            private void checkFields() {
                boolean buttonState = (Validator.validateIP(ip.getText())) &&
                        Validator.validateUsername(username.getText());

                button.setEnabled(buttonState);
            }
        }
    }
}
