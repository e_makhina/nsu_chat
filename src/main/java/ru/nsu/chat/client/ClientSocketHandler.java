package ru.nsu.chat.client;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.nsu.chat.common.message.server.ServerConnectionError;
import ru.nsu.chat.common.message.server.ServerMessage;
import ru.nsu.chat.common.serialization.Serializer;
import ru.nsu.chat.common.serialization.SerializerException;
import ru.nsu.chat.common.serialization.XMLSerializer;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.util.LinkedList;
import java.util.concurrent.BlockingQueue;

public class ClientSocketHandler {
    private static final Logger logger = LoggerFactory.getLogger(ClientSocketHandler.class);

    private Thread reader;
    private Thread writer;
    private BlockingQueue<ru.nsu.chat.common.message.client.ClientMessage> messagesToWrite;

    public ClientSocketHandler(Socket socket, BlockingQueue<ServerMessage> messagesToRead,
                               BlockingQueue<ru.nsu.chat.common.message.client.ClientMessage> messagesToWrite,
                               LinkedList<ru.nsu.chat.common.message.client.ClientMessage> unresponsedRequests) {
        this.messagesToWrite = messagesToWrite;
        this.reader = new Thread(new SocketReader(socket, messagesToRead));
        this.writer = new Thread(new SocketWriter(socket, messagesToWrite,
                messagesToRead, unresponsedRequests));

        reader.start();
        writer.start();
    }

    public void stop() {
        reader.interrupt();
        writer.interrupt();
    }

    public void sendMessage(ru.nsu.chat.common.message.client.ClientMessage message) {
        try {
            messagesToWrite.put(message);
        } catch (InterruptedException e) {
            logger.warn("Client was interrupted while sending a message");
        }
    }

    private class SocketReader implements Runnable {
        private Socket socket;
        private BlockingQueue<ServerMessage> messagesToRead;
        private Serializer deserializer;

        SocketReader(Socket socket, BlockingQueue<ServerMessage> messagesToRead) {
            this.socket = socket;
            this.messagesToRead = messagesToRead;
            deserializer = new XMLSerializer();
        }

        @Override
        public void run() {
            try {
                ServerMessage message;
                DataInputStream inputStream = new DataInputStream(socket.getInputStream());

                while (!Thread.currentThread().isInterrupted()) {
                    message = (ServerMessage) deserializer.readMessage(inputStream);
                    messagesToRead.add(message);
                }
            } catch (SerializerException e) {
                logger.warn("Socket reader got serializer exception", e);

//                messagesToRead.add(new ServerConnectionError());
            } catch (IOException e) {
                logger.warn("Could not get socket input stream");
            }
        }
    }

    private class SocketWriter implements Runnable {
        private Socket socket;
        private BlockingQueue<ru.nsu.chat.common.message.client.ClientMessage> messagesToWrite;
        private BlockingQueue<ServerMessage> messagesToRead;
        private LinkedList<ru.nsu.chat.common.message.client.ClientMessage> unrespondedRequests;
        private Serializer serializer;

        SocketWriter(Socket socket, BlockingQueue<ru.nsu.chat.common.message.client.ClientMessage> messagesToWrite,
                     BlockingQueue<ServerMessage> messagesToRead,
                     LinkedList<ru.nsu.chat.common.message.client.ClientMessage> unrespondedRequests) {
            this.socket = socket;
            this.messagesToWrite = messagesToWrite;
            this.messagesToRead = messagesToRead;
            this.unrespondedRequests = unrespondedRequests;
            this.serializer = new XMLSerializer();
        }

        @Override
        public void run() {
            try {
                ru.nsu.chat.common.message.client.ClientMessage message;
                DataOutputStream outputStream = new DataOutputStream(socket.getOutputStream());

                while (!Thread.currentThread().isInterrupted()) {
                    message = messagesToWrite.take();
                    serializer.writeMessage(outputStream, message);
                    unrespondedRequests.add(message);
                }
            } catch (SerializerException e) {
                logger.warn("Serializer exception in client");
                messagesToRead.add(new ServerConnectionError());
            } catch (InterruptedException e) {
                logger.warn("Socket writer was interrupted");
            } catch (IOException e) {
                logger.warn("Could not get socket output stream");
            }
        }
    }
}
