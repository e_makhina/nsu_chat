package ru.nsu.chat.client;

import ru.nsu.chat.common.message.server.ServerMessage;

public class ObservedEvent {
    private ServerEvent eventType;
    private ru.nsu.chat.common.message.server.ServerMessage message;

    public ObservedEvent(ServerEvent eventType, ServerMessage message) {
        this.eventType = eventType;
        this.message = message;
    }

    public ServerEvent getEventType() {
        return eventType;
    }

    public ru.nsu.chat.common.message.server.ServerMessage getMessage() {
        return message;
    }
}
