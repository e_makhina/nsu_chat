package ru.nsu.chat.client;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.nsu.chat.common.message.ClientMessageController;
import ru.nsu.chat.common.message.client.*;
import ru.nsu.chat.common.message.server.*;

import java.util.LinkedList;
import java.util.Observable;
import java.util.concurrent.BlockingQueue;

public class ClientMessageHandler extends Observable implements ClientMessageController, Runnable {
    private static final Logger logger = LoggerFactory.getLogger(ClientMessageHandler.class);

    private BlockingQueue<ServerMessage> messagesToRead;
    private BlockingQueue<ru.nsu.chat.common.message.client.ClientMessage> messagesToWrite;
    private LinkedList<ru.nsu.chat.common.message.client.ClientMessage> unrespondedRequests;

    ClientMessageHandler(BlockingQueue<ServerMessage> messagesToRead,
                         BlockingQueue<ru.nsu.chat.common.message.client.ClientMessage> messagesToWrite,
                         LinkedList<ru.nsu.chat.common.message.client.ClientMessage> unrespondedRequests) {
        this.messagesToRead = messagesToRead;
        this.messagesToWrite = messagesToWrite;
        this.unrespondedRequests = unrespondedRequests;
    }

    @Override
    public void run() {
        try {
            while (!Thread.currentThread().isInterrupted()) {
                ServerMessage message = messagesToRead.take();
                logger.info("Got " + message.getClass().getName() + "from server");
                message.handle(this);
            }
        } catch (InterruptedException e) {
            logger.warn("Client message handler was interrupted");
        }
    }

    void registerObserver(ClientContentPane contentPane) {
        logger.info("Client message handler registering new observing frame");
        deleteObservers();
        addObserver(contentPane);
    }

    void addMessage(ru.nsu.chat.common.message.client.ClientMessage message) {
        try {
            messagesToWrite.put(message);
        } catch (InterruptedException e) {
            logger.warn("Client message handler was interrupted while sending a message");
        }
    }

    @Override
    public void handleEventUserLogin(ServerMessageUserLogin message) {
        logger.info("Proceeding user login event");
        super.setChanged();
        notifyObservers(new ObservedEvent(ServerEvent.NEW_USER_LOGIN, message));
    }

    @Override
    public void handleEventUserLogout(ServerMessageUserLogout message) {
        logger.info("Proceeding user logout event");
        super.setChanged();
        notifyObservers(new ObservedEvent(ServerEvent.NEW_USER_LOGOUT, message));
    }

    @Override
    public void handleEventTextMessage(ServerMessageText message) {
        logger.info("Proceeding text message event");
        super.setChanged();
        notifyObservers(new ObservedEvent(ServerEvent.NEW_TEXT_MESSAGE, message));
    }

    @Override
    public void handleSuccessResponse(ServerSuccessMessageResponse message) {
        logger.info("Proceeding success response from server");

        ru.nsu.chat.common.message.client.ClientMessage request = unrespondedRequests.poll();
        logger.info("First unresponded request is " + request.getClass().getName());

        if (request instanceof ru.nsu.chat.common.message.client.ClientLogoutRequest) {
            handleSuccessLogoutResponse(message);
        } else if (request instanceof ru.nsu.chat.common.message.client.ClientMessageRequest) {
            handleSuccessMessageResponse(message);
        }
    }

    @Override
    public void handleSuccessLoginResponse(ServerSuccessLoginResponse message) {
        logger.info("Proceeding success login response");
        unrespondedRequests.poll();
        super.setChanged();
        notifyObservers(new ObservedEvent(ServerEvent.SUCCESS_LOGIN_RESPONSE, message));
        logger.info("Sending user list request to server");
        addMessage(new ClientUserListRequest(message.getID()));
    }

    @Override
    public void handleSuccessLogoutResponse(ServerSuccessMessageResponse message) {
        super.setChanged();
        notifyObservers(new ObservedEvent(ServerEvent.SUCCESS_LOGOUT_RESPONSE, message));
    }

    @Override
    public void handleSuccessMessageResponse(ServerSuccessMessageResponse message) {
        logger.info("Proceeding success message response");
        super.setChanged();
        notifyObservers(new ObservedEvent(ServerEvent.SUCCESS_MESSAGE_RESPONSE, message));
    }

    @Override
    public void handleSuccessUserListResponse(ServerSuccessUserListResponse message) {
        logger.info("Proceeding success user list response");
        unrespondedRequests.poll();
        super.setChanged();
        notifyObservers(new ObservedEvent(ServerEvent.SUCCESS_USERLIST_RESPONSE, message));
    }

    @Override
    public void handleErrorResponse(ServerErrorResponse message) {
        logger.info("Proceeding error response from server");

        ru.nsu.chat.common.message.client.ClientMessage request = unrespondedRequests.poll();
        logger.info("First unresponded request is " + request.getClass().getName());

        // TODO: map for errors' handlers
        if (request instanceof ru.nsu.chat.common.message.client.ClientLoginRequest) {
            handleErrorLoginResponse(message);
        } else if (request instanceof ru.nsu.chat.common.message.client.ClientLogoutRequest) {
            handleErrorLogoutResponse(message);
        } else if (request instanceof ClientUserListRequest) {
            handleErrorUserListResponse(message);
        } else if (request instanceof ru.nsu.chat.common.message.client.ClientMessageRequest) {
            handleErrorMessageResponse(message);
        }
    }

    @Override
    public void handleErrorLoginResponse(ServerErrorResponse message) {
        logger.info("Proceeding login error response");
        super.setChanged();
        notifyObservers(new ObservedEvent(ServerEvent.ERROR_LOGIN_RESPONSE, message));
    }

    @Override
    public void handleErrorLogoutResponse(ServerErrorResponse message) {
        logger.info("Proceeding logout error response");
        super.setChanged();
        notifyObservers(new ObservedEvent(ServerEvent.ERROR_LOGOUT_RESPONSE, message));
    }

    @Override
    public void handleErrorMessageResponse(ServerErrorResponse message) {
        logger.info("Proceeding text message error response");
        super.setChanged();
        notifyObservers(new ObservedEvent(ServerEvent.ERROR_MESSAGE_RESPONSE, message));
    }

    @Override
    public void handleErrorUserListResponse(ServerErrorResponse message) {
        logger.info("Proceeding user list error response");
        super.setChanged();
        notifyObservers(new ObservedEvent(ServerEvent.ERROR_USERLIST_RESPONSE, message));
    }

    @Override
    public void handleConnectionError(ServerConnectionError message) {
        logger.warn("Proceeding connection error");
        super.setChanged();
        notifyObservers(new ObservedEvent(ServerEvent.CONNECTION_ERROR, message));
    }
}
