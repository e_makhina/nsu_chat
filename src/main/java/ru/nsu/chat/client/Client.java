package ru.nsu.chat.client;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.nsu.chat.client.frame.LoginFrame;
import ru.nsu.chat.client.frame.MainFrame;
import ru.nsu.chat.common.message.client.ClientMessage;
import ru.nsu.chat.common.message.server.ServerMessage;

import java.io.IOException;
import java.net.Socket;
import java.util.LinkedList;
import java.util.Observable;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

public class Client extends Observable {
    private static final int MAX_CONNECTION_ATTEMPTS_NUM = 5;
    private static final Logger logger = LoggerFactory.getLogger(Client.class);

    private ClientMessageHandler messageHandler;
    private Thread handlingThread;
    private Socket socket;
    private ClientSocketHandler socketHandler;
    private BlockingQueue<ServerMessage> messagesToRead;
    private BlockingQueue<ClientMessage> messagesToWrite;
    private LinkedList<ClientMessage> unrespondedMessages;

    private String username;
    private String ID;

    public Client() {
        messagesToRead = new LinkedBlockingQueue<>();
        messagesToWrite = new LinkedBlockingQueue<>();
        unrespondedMessages = new LinkedList<>();

        messageHandler = new ClientMessageHandler(messagesToRead, messagesToWrite,
                unrespondedMessages);
        handlingThread = new Thread(messageHandler);
        handlingThread.start();
        logger.info("Started client");
    }

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public String getUsername() {
        return username;
    }

    public void connect(String host, int port, String username) {
        logger.info("Connecting to server");
        this.username = username;

        int attemptsNum = 0;
        while (attemptsNum < MAX_CONNECTION_ATTEMPTS_NUM) {
            try {
                socket = new Socket(host, port);
                break;
            } catch (IOException e) {
                try {
                    attemptsNum++;
                    Thread.sleep(500);
                } catch (InterruptedException e1) {
                    logger.info("Client was interrupted while connecting to server");
                }
            }
        }

        if (attemptsNum == MAX_CONNECTION_ATTEMPTS_NUM) {
            logger.warn("Could not connect to server socket");
            super.setChanged();
            notifyObservers(new ObservedEvent(ServerEvent.CONNECTION_ERROR, null));

            return;
        }

        logger.info("Connected to server");
        socketHandler = new ClientSocketHandler(socket, messagesToRead,
                messagesToWrite, unrespondedMessages);
    }

    public void disconnect() {
        try {
            socket.close();
            socketHandler.stop();
        } catch (IOException e) {
            logger.warn("IO error while closing client socket");
        }
    }

    public void sendMessage(ru.nsu.chat.common.message.client.ClientMessage message) {
        messageHandler.addMessage(message);
        logger.info("Sending " + message.getClass().getName() + " to server");
    }

    public void proceedLoginFrame() {
        LoginFrame login = new LoginFrame(this);
        messageHandler.registerObserver(login.getContentPane());
        addObserver(login.getContentPane());
    }

    public void proceedMainFrame() {
        MainFrame main = new MainFrame(this);
        messageHandler.registerObserver(main.getContentPane());
        addObserver(main.getContentPane());
    }

    public static void main(String[] args) {
        Client client = new Client();
        client.proceedLoginFrame();
    }
}
