package ru.nsu.chat.common;

import com.thoughtworks.xstream.annotations.XStreamAlias;

public class ListedUser {
    @XStreamAlias("name")
    private String username;

    public ListedUser(String username) {
        this.username = username;
    }

    public String getUsername() {
        return username;
    }
}
