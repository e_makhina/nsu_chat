package ru.nsu.chat.common.serialization;

public class SerializerException extends Exception {
    public SerializerException(Throwable throwable) {
        super(throwable);
    }

    public SerializerException(String message) {
        super(message);
    }

    public SerializerException(String message, Throwable throwable) {
        super(message, throwable);
    }
}
