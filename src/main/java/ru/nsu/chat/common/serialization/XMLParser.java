package ru.nsu.chat.common.serialization;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import ru.nsu.chat.common.ListedUser;
import ru.nsu.chat.common.message.*;
import ru.nsu.chat.common.message.client.ClientLoginRequest;
import ru.nsu.chat.common.message.client.ClientLogoutRequest;
import ru.nsu.chat.common.message.client.ClientMessageRequest;
import ru.nsu.chat.common.message.client.ClientUserListRequest;
import ru.nsu.chat.common.message.server.*;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

class XMLParser {
    private static final String TAG_COMMAND = "command";
    private static final String TAG_EVENT = "event";
    private static final String TAG_SUCCESS = "success";
    private static final String TAG_ERROR = "error";
    private static final String TAG_NAME = "name";
    private static final String TAG_SESSION = "session";
    private static final String TAG_MESSAGE = "message";

    private HashMap<String, XMLTagParser> mainTags;
    private Document document;
    private Element topElement;

    XMLParser() {
        mainTags = new HashMap<>();
        mainTags.put(TAG_COMMAND, new CommandTagParser());
        mainTags.put(TAG_EVENT, new EventTagParser());
        mainTags.put(TAG_SUCCESS, new SuccessTagParser());
        mainTags.put(TAG_ERROR, new ErrorTagParser());
    }

    Message parse(String message) throws XMLParserException {
        try {
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();

            document = builder.parse(new ByteArrayInputStream(message.getBytes()));
            topElement = document.getDocumentElement();
            String mainTag = topElement.getTagName();

            XMLTagParser tagParser = mainTags.get(mainTag);
            if (tagParser != null) {
                return tagParser.parse();
            } else {
                throw new XMLParserException();
            }
        } catch (ParserConfigurationException | SAXException | IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    private class CommandTagParser implements XMLTagParser {
        private static final String TAG_LOGIN = "login";
        private static final String TAG_LOGOUT = "logout";
        private static final String TAG_MESSAGE = "message";
        private static final String TAG_USERLIST = "list";

        private HashMap<String, XMLTagParser> attributeTags;

        public CommandTagParser() {
            attributeTags = new HashMap<>();
            attributeTags.put(TAG_LOGIN, new CommandLoginTagParser());
            attributeTags.put(TAG_LOGOUT, new CommandLogoutTagParser());
            attributeTags.put(TAG_MESSAGE, new CommandMessageTagParser());
            attributeTags.put(TAG_USERLIST, new CommandListTagParser());
        }

        @Override
        public Message parse() throws XMLParserException {
            String messageType = topElement.getAttribute(TAG_NAME);
            XMLTagParser tagParser = attributeTags.get(messageType);

            if (tagParser != null) {
                return tagParser.parse();
            } else {
                throw new XMLParserException();
            }
        }

        private class CommandLoginTagParser implements XMLTagParser {
            @Override
            public Message parse() throws XMLParserException {
                String name = document.getElementsByTagName(TAG_NAME).item(0).getTextContent();

                return new ClientLoginRequest(name);
            }
        }

        private class CommandLogoutTagParser implements XMLTagParser {
            @Override
            public Message parse() throws XMLParserException {
                String session = document.getElementsByTagName(TAG_SESSION).item(0).getTextContent();

                return new ClientLogoutRequest(session);
            }
        }

        private class CommandMessageTagParser implements XMLTagParser {
            @Override
            public Message parse() throws XMLParserException {
                String message = document.getElementsByTagName(TAG_MESSAGE).item(0).getTextContent();
                String session = document.getElementsByTagName(TAG_SESSION).item(0).getTextContent();

                return new ClientMessageRequest(message, session);
            }
        }

        private class CommandListTagParser implements XMLTagParser {
            @Override
            public Message parse() throws XMLParserException {
                String session = document.getElementsByTagName(TAG_SESSION).item(0).getTextContent();

                return new ClientUserListRequest(session);
            }
        }
    }

    private class EventTagParser implements XMLTagParser {
        private static final String TAG_USERLOGIN = "userlogin";
        private static final String TAG_USERLOGOUT = "userlogout";

        private HashMap<String, XMLTagParser> attributeTags;

        public EventTagParser() {
            attributeTags = new HashMap<>();
            attributeTags.put(TAG_MESSAGE, new EventMessageTagParser());
            attributeTags.put(TAG_USERLOGIN, new EventUserLoginTagParser());
            attributeTags.put(TAG_USERLOGOUT, new EventUserLogoutTagParser());
        }

        @Override
        public Message parse() throws XMLParserException {
            String messageType = topElement.getAttribute(TAG_NAME);
            XMLTagParser tagParser = attributeTags.get(messageType);

            if (tagParser != null) {
                return tagParser.parse();
            } else {
                throw new XMLParserException();
            }
        }

        private class EventMessageTagParser implements XMLTagParser {
            @Override
            public Message parse() throws XMLParserException {
                String message = document.getElementsByTagName(TAG_MESSAGE).item(0).getTextContent();
                String name = document.getElementsByTagName(TAG_NAME).item(0).getTextContent();

                return new ServerMessageText(message, name);
            }
        }

        private class EventUserLoginTagParser implements XMLTagParser {
            @Override
            public Message parse() throws XMLParserException {
                String name = document.getElementsByTagName(TAG_NAME).item(0).getTextContent();

                return new ServerMessageUserLogin(name);
            }
        }

        private class EventUserLogoutTagParser implements XMLTagParser {
            @Override
            public Message parse() throws XMLParserException {
                String name = document.getElementsByTagName(TAG_NAME).item(0).getTextContent();

                return new ServerMessageUserLogout(name);
            }
        }
    }

    private class SuccessTagParser implements XMLTagParser {
        private static final String TAG_USERLIST = "listusers";

        private HashMap<String, XMLTagParser> attributeTags;

        public SuccessTagParser() {
            attributeTags = new HashMap<>();
            attributeTags.put(TAG_USERLIST, new SuccessUserListTagParser());
            attributeTags.put(TAG_SESSION, new SuccessSessionTagParser());
        }

        @Override
        public Message parse() throws XMLParserException {
            NodeList list = topElement.getChildNodes();
            if (list.getLength() == 0) {
                return new ServerSuccessMessageResponse();
            }

            String messageType = list.item(0).getNodeName();
            XMLTagParser tagParser = attributeTags.get(messageType);
            if (tagParser != null) {
                return tagParser.parse();
            } else {
                throw new XMLParserException();
            }
        }

        private class SuccessUserListTagParser implements XMLTagParser {
            @Override
            public Message parse() throws XMLParserException {
                ArrayList<ListedUser> list = new ArrayList<>();
                NodeList names = document.getElementsByTagName(TAG_NAME);

                for (int i = 0; i < names.getLength(); i++) {
                    String name = names.item(i).getTextContent();
                    list.add(new ListedUser(name));
                }

                return new ServerSuccessUserListResponse(list);
            }
        }

        private class SuccessSessionTagParser implements XMLTagParser {
            @Override
            public Message parse() throws XMLParserException {
                String session = document.getElementsByTagName(TAG_SESSION).item(0).getTextContent();

                return new ServerSuccessLoginResponse(session);
            }
        }
    }

    private class ErrorTagParser implements XMLTagParser {
        @Override
        public Message parse() throws XMLParserException {
            String message = document.getElementsByTagName(TAG_MESSAGE).item(0).getTextContent();

            return new ServerErrorResponse(message);
        }
    }
}
