package ru.nsu.chat.common.serialization;

import ru.nsu.chat.common.message.Message;

import java.io.InputStream;
import java.io.OutputStream;

public interface Serializer {
    Message readMessage(InputStream inputStream) throws SerializerException;

    void writeMessage(OutputStream outputStream, Message message) throws SerializerException;
}
