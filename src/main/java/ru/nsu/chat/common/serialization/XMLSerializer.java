package ru.nsu.chat.common.serialization;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.annotations.Annotations;
import com.thoughtworks.xstream.io.xml.StaxDriver;
import com.thoughtworks.xstream.io.xml.StaxWriter;
import ru.nsu.chat.common.message.Message;

import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;
import java.io.*;

public class XMLSerializer implements Serializer {
    public Message readMessage(InputStream inputStream) throws SerializerException {
        Message message;
        XMLParser parser = new XMLParser();

        DataInputStream dataInputStream = new DataInputStream(inputStream);

        try {
            int size = dataInputStream.readInt();
            if (size <= 0) {
                throw new XMLParserException();
            }

            byte[] bytes = new byte[size];
            dataInputStream.readFully(bytes);
            message = parser.parse(new String(bytes));
        } catch (XMLParserException e) {
            throw new SerializerException(e);
        } catch (IOException e) {
            throw new SerializerException("Could not read message from input stream. ", e);
        }

        return message;
    }

    @SuppressWarnings("deprecation")
    public void writeMessage(OutputStream outputStream, Message message) throws SerializerException {
        try {
            DataOutputStream dataOutputStream = new DataOutputStream(outputStream);

            StaxDriver driver = new StaxDriver() {
                @Override
                public StaxWriter createStaxWriter(XMLStreamWriter writer) throws XMLStreamException {
                    return super.createStaxWriter(writer, false);
                }
            };

            XStream stream = new XStream(driver);
            Annotations.configureAliases(stream, message.getClass());

            String convertedMessage = stream.toXML(message);
            dataOutputStream.writeInt(convertedMessage.getBytes().length);
            dataOutputStream.write(convertedMessage.getBytes());

            dataOutputStream.flush();
        } catch (IOException e) {
            throw new SerializerException("Could not send message via input stream. ", e);
        }
    }
}
