package ru.nsu.chat.common.serialization;

import ru.nsu.chat.common.message.Message;

public interface XMLTagParser {
    Message parse() throws XMLParserException;
}
