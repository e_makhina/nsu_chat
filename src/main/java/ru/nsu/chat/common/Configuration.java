package ru.nsu.chat.common;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class Configuration {
    private int XMLPort;

    public Configuration(int XMLPort) {
        this.XMLPort = XMLPort;
    }

    public int getXMLPort() {
        return XMLPort;
    }

    public static Configuration parse()  {
        Properties properties = new Properties();

        String propertyFileName = "config.properties";

        try {
            InputStream inputStream = ClassLoader.getSystemResourceAsStream(propertyFileName);
            properties.load(inputStream);

            return new Configuration(Integer.parseInt(properties.getProperty("XMLPort")));
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }
}
