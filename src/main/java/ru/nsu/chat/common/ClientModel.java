package ru.nsu.chat.common;

import ru.nsu.chat.common.message.server.ServerMessage;

public interface ClientModel {
    String getUsername();

    String getID();

    void setUsername(String username);

    void sendMessage(ServerMessage message);

    void disconnect();
}
