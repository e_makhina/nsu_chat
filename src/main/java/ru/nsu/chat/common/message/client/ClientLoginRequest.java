package ru.nsu.chat.common.message.client;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamAsAttribute;
import ru.nsu.chat.common.ClientModel;
import ru.nsu.chat.common.message.ServerMessageController;

@XStreamAlias("command")
public class ClientLoginRequest implements ClientMessage {
    @XStreamAlias("name")
    private String ID;

    @XStreamAsAttribute
    private String name = "login";

    public ClientLoginRequest(String ID) {
        this.ID = ID;
    }

    public String getID() {
        return ID;
    }

    @Override
    public void handle(ClientModel client, ServerMessageController controller) {
        controller.handleLoginRequest(client, this);
    }
}
