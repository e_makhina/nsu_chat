package ru.nsu.chat.common.message.client;

import ru.nsu.chat.common.ClientModel;
import ru.nsu.chat.common.message.Message;
import ru.nsu.chat.common.message.ServerMessageController;

public interface ClientMessage extends Message {
    void handle(ClientModel client, ServerMessageController controller);
}
