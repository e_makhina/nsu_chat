package ru.nsu.chat.common.message.server;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamAsAttribute;
import ru.nsu.chat.common.message.ClientMessageController;

@XStreamAlias("event")
public class ServerMessageText implements ServerMessage {
    @XStreamAlias("message")
    private String message;

    @XStreamAlias("name")
    private String username;

    @XStreamAsAttribute
    private String name = "message";

    public ServerMessageText(String message, String username) {
        this.message = message;
        this.username = username;
    }

    public String getMessage() {
        return message;
    }

    public String getUsername() {
        return username;
    }

    @Override
    public void handle(ClientMessageController controller) {
        controller.handleEventTextMessage(this);
    }
}
