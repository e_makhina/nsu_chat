package ru.nsu.chat.common.message.server;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import ru.nsu.chat.common.message.ClientMessageController;

@XStreamAlias("error")
public class ServerErrorResponse implements ServerMessage {
    @XStreamAlias("message")
    private String reason;

    public ServerErrorResponse(String reason) {
        this.reason = reason;
    }

    public String getReason() {
        return reason;
    }

    @Override
    public void handle(ClientMessageController controller) {
        controller.handleErrorResponse(this);
    }
}
