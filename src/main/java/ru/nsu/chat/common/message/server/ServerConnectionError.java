package ru.nsu.chat.common.message.server;

import ru.nsu.chat.common.message.ClientMessageController;

public class ServerConnectionError implements ServerMessage {
    @Override
    public void handle(ClientMessageController controller) {
        controller.handleConnectionError(this);
    }
}
