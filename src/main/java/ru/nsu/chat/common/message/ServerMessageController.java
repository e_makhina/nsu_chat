package ru.nsu.chat.common.message;

import ru.nsu.chat.common.ClientModel;
import ru.nsu.chat.common.message.client.ClientMessageRequest;
import ru.nsu.chat.common.message.client.ClientUserListRequest;

public interface ServerMessageController extends MessageController {
    void handleLoginRequest(ClientModel client, ru.nsu.chat.common.message.client.ClientLoginRequest clientLoginRequest);

    void handleLogoutRequest(ClientModel client, ru.nsu.chat.common.message.client.ClientLogoutRequest clientLogoutRequest);

    void handleMessageRequest(ClientModel client, ClientMessageRequest clientMessageRequest);

    void handleUserListRequest(ClientModel client, ClientUserListRequest clientUserListRequest);
}
