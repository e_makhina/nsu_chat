package ru.nsu.chat.common.message.server;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamAsAttribute;
import ru.nsu.chat.common.message.ClientMessageController;

@XStreamAlias("event")
public class ServerMessageUserLogin implements ru.nsu.chat.common.message.server.ServerMessage {
    @XStreamAlias("name")
    private String username;

    @XStreamAsAttribute
    private String name = "userlogin";

    public ServerMessageUserLogin(String username) {
        this.username = username;
    }

    public String getUsername() {
        return username;
    }

    @Override
    public void handle(ClientMessageController controller) {
        controller.handleEventUserLogin(this);
    }
}
