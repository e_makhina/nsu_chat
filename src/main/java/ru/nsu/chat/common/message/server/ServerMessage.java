package ru.nsu.chat.common.message.server;

import ru.nsu.chat.common.message.ClientMessageController;
import ru.nsu.chat.common.message.Message;

public interface ServerMessage extends Message {
    void handle(ClientMessageController controller);
}
