package ru.nsu.chat.common.message;

import ru.nsu.chat.common.message.server.*;

public interface ClientMessageController extends MessageController {
    void handleEventUserLogin(ServerMessageUserLogin message);

    void handleEventUserLogout(ServerMessageUserLogout message);

    void handleEventTextMessage(ServerMessageText message);

    void handleSuccessResponse(ServerSuccessMessageResponse message);

    void handleSuccessLoginResponse(ServerSuccessLoginResponse message);

    void handleSuccessLogoutResponse(ServerSuccessMessageResponse message);

    void handleSuccessMessageResponse(ServerSuccessMessageResponse message);

    void handleSuccessUserListResponse(ServerSuccessUserListResponse message);

    void handleErrorResponse(ServerErrorResponse message);

    void handleErrorLoginResponse(ServerErrorResponse message);

    void handleErrorLogoutResponse(ServerErrorResponse message);

    void handleErrorMessageResponse(ServerErrorResponse message);

    void handleErrorUserListResponse(ServerErrorResponse message);

    void handleConnectionError(ServerConnectionError message);
}
