package ru.nsu.chat.common.message.server;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;
import ru.nsu.chat.common.ListedUser;
import ru.nsu.chat.common.message.ClientMessageController;

import java.util.ArrayList;

@XStreamAlias("success")
public class ServerSuccessUserListResponse implements ru.nsu.chat.common.message.server.ServerMessage {
    @XStreamAlias("listusers")
    private Users users;

    public ServerSuccessUserListResponse(ArrayList<ListedUser> users) {
        this.users = new Users(users);
    }

    @Override
    public void handle(ClientMessageController controller) {
        controller.handleSuccessUserListResponse(this);
    }

    public ArrayList<ListedUser> getUsers() {
        return users.getUsers();
    }

    public static class Users {
        @XStreamImplicit(itemFieldName = "user")
        private final ArrayList<ListedUser> users;

        Users(ArrayList<ListedUser> users) {
            this.users = users;
        }

        ArrayList<ListedUser> getUsers() {
            return users;
        }
    }
}
