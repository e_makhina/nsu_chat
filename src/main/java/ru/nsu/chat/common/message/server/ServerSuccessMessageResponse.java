package ru.nsu.chat.common.message.server;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import ru.nsu.chat.common.message.ClientMessageController;

@XStreamAlias("success")
public class ServerSuccessMessageResponse implements ServerMessage {
    @Override
    public void handle(ClientMessageController controller) {
        controller.handleSuccessResponse(this);
    }
}
