package ru.nsu.chat.common.message.client;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamAsAttribute;
import ru.nsu.chat.common.ClientModel;
import ru.nsu.chat.common.message.ServerMessageController;

@XStreamAlias("command")
public class ClientMessageRequest implements ClientMessage {
    @XStreamAlias("message")
    private String message;

    @XStreamAlias("session")
    private String ID;

    @XStreamAsAttribute
    private String name = "message";

    public ClientMessageRequest(String message, String ID) {
        this.message = message;
        this.ID = ID;
    }

    public String getMessage() {
        return message;
    }

    public String getID() {
        return ID;
    }

    @Override
    public void handle(ClientModel client, ServerMessageController controller) {
        controller.handleMessageRequest(client, this);
    }
}
