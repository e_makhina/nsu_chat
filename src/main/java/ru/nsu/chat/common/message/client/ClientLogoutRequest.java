package ru.nsu.chat.common.message.client;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamAsAttribute;
import ru.nsu.chat.common.ClientModel;
import ru.nsu.chat.common.message.ServerMessageController;

@XStreamAlias("command")
public class ClientLogoutRequest implements ClientMessage {
    @XStreamAlias("session")
    private String ID;

    @XStreamAsAttribute
    private String name = "logout";

    public ClientLogoutRequest(String ID) {
        this.ID = ID;
    }

    public String getID() {
        return ID;
    }

    @Override
    public void handle(ClientModel client, ServerMessageController controller) {
        controller.handleLogoutRequest(client, this);
    }
}
