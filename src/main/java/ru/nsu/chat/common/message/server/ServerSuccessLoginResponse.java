package ru.nsu.chat.common.message.server;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import ru.nsu.chat.common.message.ClientMessageController;

@XStreamAlias("success")
public class ServerSuccessLoginResponse implements ServerMessage {
    @XStreamAlias("session")
    private String ID;

    public ServerSuccessLoginResponse(String ID) {
        this.ID = ID;
    }

    public String getID() {
        return ID;
    }

    @Override
    public void handle(ClientMessageController controller) {
        controller.handleSuccessLoginResponse(this);
    }
}
