package ru.nsu.chat.server;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.nsu.chat.common.message.client.ClientLogoutRequest;
import ru.nsu.chat.common.message.client.ClientMessage;
import ru.nsu.chat.common.message.server.ServerMessage;
import ru.nsu.chat.common.serialization.Serializer;
import ru.nsu.chat.common.serialization.SerializerException;
import ru.nsu.chat.common.serialization.XMLSerializer;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

public class ServerSocketHandler {
    private static final Logger logger = LoggerFactory.getLogger(ServerSocketHandler.class);

    private Thread reader;
    private Thread writer;
    private BlockingQueue<ServerMessage> messagesToWrite;
    private ServerMessageHandler messageHandler;

    public ServerSocketHandler(Socket socket, BlockingQueue<MessageFromClient> messagesToRead,
                               ServerMessageHandler handler, ClientFromServerView client) {
        messagesToWrite = new LinkedBlockingQueue<>();
        this.messageHandler = handler;

        reader = new Thread(new SocketReader(socket, messagesToRead, client));
        writer = new Thread(new SocketWriter(socket, messagesToWrite, client));

        reader.start();
        writer.start();
    }

    public void stop() {
        reader.interrupt();
        writer.interrupt();
    }

    public void sendMessage(ServerMessage message) {
        try {
            messagesToWrite.put(message);
        } catch (InterruptedException e) {
            logger.warn("Server was interrupted while sending message");
        }
    }

    private class SocketReader implements Runnable {
        private Socket socket;
        private BlockingQueue<MessageFromClient> messagesToRead;
        private ClientFromServerView client;
        private Serializer deserializer;

        SocketReader(Socket socket, BlockingQueue<MessageFromClient> messagesToRead,
                     ClientFromServerView client) {
            this.socket = socket;
            this.messagesToRead = messagesToRead;
            this.client = client;

            this.deserializer = new XMLSerializer();
        }

        @Override
        public void run() {
            try {
                ClientMessage message;
                DataInputStream inputStream = new DataInputStream(socket.getInputStream());

                while (!Thread.currentThread().isInterrupted()) {
                    message = (ClientMessage) deserializer.readMessage(inputStream);
                    messagesToRead.add(new MessageFromClient(client, message));
                }
            } catch (SerializerException e) {
                logger.warn("Socket reader got serializer exception", e);

                messagesToRead.add(new MessageFromClient(client, new ClientLogoutRequest(client.getID())));
            } catch (IOException e) {
                logger.warn("Could not get socket input stream");
            }
        }
    }

    private static class SocketWriter implements Runnable {
        private Socket socket;
        private BlockingQueue<ServerMessage> messagesToWrite;
        private ClientFromServerView client;
        private Serializer serializer;

        SocketWriter(Socket socket, BlockingQueue<ServerMessage> messagesToWrite,
                     ClientFromServerView client) {
            this.socket = socket;
            this.messagesToWrite = messagesToWrite;
            this.client = client;

            this.serializer = new XMLSerializer();
        }

        void sendMessage(ServerMessage message) {
            try {
                messagesToWrite.put(message);
            } catch (InterruptedException e) {
                logger.warn("Socket writer was interrupted");
            }
        }

        @Override
        public void run() {
            try {
                ServerMessage message;
                DataOutputStream outputStream = new DataOutputStream(socket.getOutputStream());

                while (!Thread.currentThread().isInterrupted()) {
                    message = messagesToWrite.take();
                    serializer.writeMessage(outputStream, message);
                }
            } catch (SerializerException e) {
                logger.warn(client.getUsername() + ": lost connection to client");
            } catch (InterruptedException e) {
                logger.warn("Socket writer was interrupted");
            } catch (IOException e) {
                logger.warn("Could not get socket output stream");
            }
        }
    }
}
