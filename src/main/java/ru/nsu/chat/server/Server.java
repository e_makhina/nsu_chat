package ru.nsu.chat.server;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.nsu.chat.common.ClientModel;
import ru.nsu.chat.common.Configuration;
import ru.nsu.chat.common.message.server.ServerMessage;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.LinkedList;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.atomic.AtomicInteger;

public class Server {
    private static final int FIRST_ID = 1;
    private static final int LAST_MESSAGES_CAPACITY = 10;
    private static final int ERROR = 1;
    private static final Logger logger = LoggerFactory.getLogger(Server.class);

    private LinkedList<ClientModel> clients;
    private Configuration configuration;
    private Thread xmlListener;
    private ServerMessageHandler messageHandler;

    private BlockingQueue<MessageFromClient> messagesToRead;
    private BlockingQueue<ServerMessage> messagesToWrite;
    private CyclicBuffer<ServerMessage> lastMessages;

    private AtomicInteger availableID;

    public Server() {
        clients = new LinkedList<>();
        configuration = Configuration.parse();
        availableID = new AtomicInteger(FIRST_ID);

        messagesToRead = new LinkedBlockingQueue<>();
        messagesToWrite = new LinkedBlockingQueue<>();
        lastMessages = new CyclicBuffer<>(LAST_MESSAGES_CAPACITY);

        messageHandler = new ServerMessageHandler(clients, messagesToRead, messagesToWrite, lastMessages);
        Thread handlingThread = new Thread(messageHandler);
        handlingThread.start();

        logger.info("Server started");
    }

    public synchronized void addClient(ClientModel client) {
        logger.info("Server: adding new client");
        clients.add(client);
        logger.info("There are " + clients.size() + " clients now");
    }

    public void startXMLListener() {
        xmlListener = new Thread("XML socket listener") {
            @Override
            public void run() {
                try {
                    int port = configuration.getXMLPort();
                    ServerSocket serverSocket = new ServerSocket(port);
                    logger.info("XML socket started on " + port + " port");

                    while (!isInterrupted()) {
                        Socket socket = serverSocket.accept();
                        logger.info("Accepted socket");

                        ClientModel newClient = new ClientFromServerView(socket,
                                messagesToRead, messageHandler, availableID.getAndIncrement());
                        clients.add(newClient);
                    }
                } catch (IOException e) {
                    logger.warn("XML port error");
                    System.exit(ERROR);
                }
            }
        };

        xmlListener.start();
        logger.info("Started XML listener");
    }

    public static void main(String[] args) {
        Server server = new Server();
        server.startXMLListener();
    }
}
