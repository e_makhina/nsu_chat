package ru.nsu.chat.server;

import ru.nsu.chat.common.ClientModel;
import ru.nsu.chat.common.message.client.ClientMessage;

public class MessageFromClient {
    private ClientModel client;
    private ClientMessage message;

    public MessageFromClient(ClientModel client, ClientMessage message) {
        this.client = client;
        this.message = message;
    }

    public ClientModel getClient() {
        return client;
    }

    public ClientMessage getMessage() {
        return message;
    }
}
