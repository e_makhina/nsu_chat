package ru.nsu.chat.server;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.nsu.chat.common.ClientModel;
import ru.nsu.chat.common.message.server.ServerMessage;

import java.io.IOException;
import java.net.Socket;
import java.util.concurrent.BlockingQueue;

public class ClientFromServerView implements ClientModel {
    private static final Logger logger = LoggerFactory.getLogger(ClientFromServerView.class);

    private String username;
    private String ID;
    private Socket socket;
    private ServerSocketHandler serverSocketHandler;

    public ClientFromServerView(Socket socket, BlockingQueue<MessageFromClient> unreadMessages,
                                ServerMessageHandler messageHandler, int ID) {
        this.socket = socket;
        serverSocketHandler = new ServerSocketHandler(socket, unreadMessages, messageHandler, this);
        this.ID = String.valueOf(ID);
    }

    @Override
    public String getUsername() {
        return username;
    }

    @Override
    public String getID() {
        return ID;
    }

    @Override
    public void setUsername(String username) {
        this.username = username;
    }

    @Override
    public void sendMessage(ServerMessage message) {
        logger.info(username + ": sending " + message.getClass().getName());
        serverSocketHandler.sendMessage(message);
    }

    @Override
    public void disconnect() {
        logger.info(ID + ": disconnecting");

        try {
            serverSocketHandler.stop();
            socket.close();
        } catch (IOException e) {
            logger.warn("Could not close socket while disconnecting user " + ID);
        }
    }
}
