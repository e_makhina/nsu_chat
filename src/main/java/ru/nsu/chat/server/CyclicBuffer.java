package ru.nsu.chat.server;

import java.util.Iterator;
import java.util.LinkedList;

public class CyclicBuffer<T> implements Iterable<T> {
    private LinkedList<T> buffer;
    private int size;

    public CyclicBuffer(int size) {
        this.size = size;
        buffer = new LinkedList<>();
    }

    public void add(T element) {
        if (buffer.size() == size) {
            buffer.pollFirst();
        }

        buffer.add(element);
    }

    @Override
    public Iterator<T> iterator() {
        return buffer.iterator();
    }
}
