package ru.nsu.chat.server;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.nsu.chat.common.ClientModel;
import ru.nsu.chat.common.Configuration;
import ru.nsu.chat.common.ListedUser;
import ru.nsu.chat.common.message.ServerMessageController;
import ru.nsu.chat.common.message.client.ClientLoginRequest;
import ru.nsu.chat.common.message.client.ClientLogoutRequest;
import ru.nsu.chat.common.message.client.ClientMessageRequest;
import ru.nsu.chat.common.message.client.ClientUserListRequest;
import ru.nsu.chat.common.message.server.*;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.concurrent.BlockingQueue;

public class ServerMessageHandler implements ServerMessageController, Runnable {
    private static final Logger logger = LoggerFactory.getLogger(ServerMessageHandler.class);

    private LinkedList<ClientModel> clients;
    private BlockingQueue<MessageFromClient> unreadMessages;
    private BlockingQueue<ServerMessage> unwrittenMessages;
    private CyclicBuffer<ServerMessage> lastMessages;
    private Configuration configuration;

    ServerMessageHandler(LinkedList<ClientModel> clients,
                         BlockingQueue<MessageFromClient> unreadMessages,
                         BlockingQueue<ServerMessage> unwrittenMessages,
                         CyclicBuffer<ServerMessage> log) {
        configuration = Configuration.parse();
        this.clients = clients;
        this.unreadMessages = unreadMessages;
        this.unwrittenMessages = unwrittenMessages;
        this.lastMessages = log;
    }

    @Override
    public void run() {
        try {
            MessageFromClient message;
            while (!Thread.currentThread().isInterrupted()) {
                message = unreadMessages.take();
                message.getMessage().handle(message.getClient(), this);

                logger.info("Message taken: " + message.getClient() +
                        ": " + message.getMessage());
            }
        } catch (InterruptedException e) {
            logger.warn("Server message handler was interrupted");
        }
    }

    public void addMessage(MessageFromClient message) {
        try {
            unreadMessages.put(message);
        } catch (InterruptedException e) {
            logger.warn("Server message handler was interrupted while sending a message");
        }
    }

    @Override
    public void handleLoginRequest(ClientModel client, ClientLoginRequest message) {
        logger.info(message.getID() + ": proceeding login request");
        if (usernameAlreadyExists(message.getID())) {
            logger.info(message.getID() + ": login request was unsuccessful");

            client.sendMessage(new ServerErrorResponse("Username already exists"));

            try {
                Thread.sleep(500);
            } catch (InterruptedException e) {
                logger.warn("Message controller was interrupted while disconnecting client");
            }

            client.disconnect();
            clients.remove(client);

            logger.info("There are " + clients.size() + " clients now");
        } else {
            logger.info(message.getID() + ": login request was unsuccessful");

            client.setUsername(message.getID());
            client.sendMessage(new ServerSuccessLoginResponse(client.getID()));

            ServerMessageUserLogin event = new ServerMessageUserLogin(message.getID());

            for (ServerMessage msg : lastMessages) {
                client.sendMessage(msg);
            }

            lastMessages.add(event);
            sendAll(event);
        }
    }

    @Override
    public void handleLogoutRequest(ClientModel client, ClientLogoutRequest message) {
        logger.info(client.getUsername() + ": proceeding logout request");

        ServerMessageUserLogout event = new ServerMessageUserLogout(client.getUsername());
        client.disconnect();
        logger.info(message.getID() + ": logout request was successful");

        clients.remove(client);
        logger.info("There are " + clients.size() + " clients now");

        client.sendMessage(new ServerSuccessMessageResponse());

        sendAll(event);
        lastMessages.add(event);
    }

    @Override
    public void handleMessageRequest(ClientModel client, ClientMessageRequest message) {
        ServerMessage event = new ServerMessageText(message.getMessage(), client.getUsername());
        client.sendMessage(new ServerSuccessMessageResponse());
        sendAll(event);
        lastMessages.add(event);

        logger.info(client.getUsername() + ": proceeding message");
    }

    @Override
    public void handleUserListRequest(ClientModel client, ClientUserListRequest message) {
        ArrayList<ListedUser> userlist = new ArrayList<>();

        for (ClientModel cl : clients) {
            userlist.add(new ListedUser(cl.getUsername()));
        }

        client.sendMessage(new ServerSuccessUserListResponse(userlist));
        logger.info(client.getUsername() + "proceeding user list request");
    }

    private boolean usernameAlreadyExists(String username) {
        for (ClientModel client : clients) {
            if (client.getUsername() != null &&
                    client.getUsername().equals(username)) {
                return true;
            }
        }

        return false;
    }

    private void sendAll(ServerMessage message) {
        for (ClientModel client : clients) {
            client.sendMessage(message);
        }
    }
}
