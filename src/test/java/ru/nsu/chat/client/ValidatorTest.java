package ru.nsu.chat.client;

import org.junit.Test;

import static org.junit.Assert.*;

public class ValidatorTest {
    @Test
    public void validateIP() throws Exception {
        assert (Validator.validateIP("127.0.0.1") == true);
        assert (Validator.validateIP("127.0.0") == false);
        assert (Validator.validateIP("asd123") == false);
        assert (Validator.validateIP("91.0.0.0.0") == false);
    }

    @Test
    public void validateUsername() throws Exception {
        assert (Validator.validateUsername("1234") == true);
        assert (Validator.validateUsername("qwerty") == true);
        assert (Validator.validateUsername("qwer_") == true);
        assert (Validator.validateUsername("123") == false);
        assert (Validator.validateUsername("qwert%") == false);
        assert (Validator.validateUsername("1234567890") == false);
    }

}