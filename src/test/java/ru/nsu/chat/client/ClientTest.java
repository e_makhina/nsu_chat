package ru.nsu.chat.client;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import ru.nsu.chat.common.message.client.ClientLoginRequest;
import ru.nsu.chat.common.message.client.ClientLogoutRequest;
import ru.nsu.chat.common.message.client.ClientMessageRequest;
import ru.nsu.chat.common.message.client.ClientUserListRequest;
import ru.nsu.chat.server.Server;

import static org.junit.Assert.*;

public class ClientTest {
    Client client;
    Server server;
    @Before
    public void setUp() throws Exception {
        server = new Server();
        server.startXMLListener();

        client = new Client();
        client.connect("127.0.0.1", 8888, "qwerty");
    }

    @Test
    public void sendMessage() throws Exception {
        client.sendMessage(new ClientLoginRequest("qwerty"));
        client.sendMessage(new ClientMessageRequest("hi", "qwerty"));
        client.sendMessage(new ClientLogoutRequest("qwerty"));
        client.sendMessage(new ClientUserListRequest("qwerty"));
    }

    @After
    public void tearDown() {
        client.disconnect();
    }

}