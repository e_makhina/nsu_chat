package ru.nsu.chat.client;

import org.junit.Before;
import org.junit.Test;
import ru.nsu.chat.common.message.server.ServerMessageText;

import static org.junit.Assert.*;

public class ObservedEventTest {
    private ObservedEvent event;
    @Before
    public void setUp() throws Exception {
        event = new ObservedEvent(ServerEvent.NEW_TEXT_MESSAGE, new ServerMessageText("hi", "qwerty"));
    }

    @Test
    public void getEventType() throws Exception {
        assert (event.getEventType() == ServerEvent.NEW_TEXT_MESSAGE);
        assert (event.getEventType() != ServerEvent.CONNECTION_ERROR);
    }

    @Test
    public void getMessage() throws Exception {
        assert (((ServerMessageText) event.getMessage()).getMessage().equals("hi"));
        assert (((ServerMessageText) event.getMessage()).getUsername().equals("qwerty"));

        assert (!((ServerMessageText) event.getMessage()).getMessage().equals("hi1"));
        assert (!((ServerMessageText) event.getMessage()).getUsername().equals("qwerty_"));
    }

}